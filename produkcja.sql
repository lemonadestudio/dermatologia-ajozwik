-- phpMyAdmin SQL Dump
-- version 4.6.1
-- http://www.phpmyadmin.net
--
-- Host: lemonade.nazwa.pl:3306
-- Czas generowania: 10 Sty 2017, 09:10
-- Wersja serwera: 5.5.53-MariaDB
-- Wersja PHP: 5.5.9-1ubuntu4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `lemonade_34`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `gallery`
--

CREATE TABLE `gallery` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `old_slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `on_list` tinyint(1) NOT NULL,
  `attachable` tinyint(1) NOT NULL,
  `seo_generate` tinyint(1) DEFAULT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `gallery`
--

INSERT INTO `gallery` (`id`, `title`, `slug`, `old_slug`, `on_list`, `attachable`, `seo_generate`, `seo_title`, `seo_keywords`, `seo_description`, `created_at`, `updated_at`) VALUES
(2, 'Galeria', 'galeria', 'galeria', 1, 1, 0, 'Galeria', NULL, NULL, '2016-10-31 12:08:55', '2016-10-31 12:08:55'),
(3, 'Certyfikaty', 'certyfikaty', 'certyfikaty', 1, 1, 0, 'Certyfikaty', NULL, NULL, '2016-12-07 21:33:25', '2016-12-07 21:33:26'),
(4, 'Aktualność - Density', 'aktualnosc-density', 'aktualnosc-density', 1, 1, 0, 'Aktualność - Density', NULL, NULL, '2017-01-02 18:20:44', '2017-01-02 18:20:50'),
(5, 'Aktualność - Princess', 'aktualnosc-princess', 'aktualnosc-princess', 1, 1, 0, 'Aktualność - Princess', NULL, NULL, '2017-01-09 17:35:14', '2017-01-09 17:35:15');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `gallery_photo`
--

CREATE TABLE `gallery_photo` (
  `id` int(11) NOT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `arrangement` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `gallery_photo`
--

INSERT INTO `gallery_photo` (`id`, `gallery_id`, `photo`, `arrangement`) VALUES
(66, 3, 'gallery-image-5848739534b6e.jpg', 1),
(67, 3, 'gallery-image-5848739547929.jpg', 2),
(68, 3, 'gallery-image-5848739556fc8.jpg', 3),
(69, 3, 'gallery-image-58487395d050e.jpg', 4),
(70, 3, 'gallery-image-58487395e3acb.jpg', 5),
(71, 3, 'gallery-image-58487396036de.jpg', 6),
(72, 3, 'gallery-image-58487396179d2.jpg', 7),
(73, 3, 'gallery-image-584873962b9fb.jpg', 8),
(74, 3, 'gallery-image-584873963f638.jpg', 9),
(75, 3, 'gallery-image-5848739652b15.jpg', 10),
(76, 3, 'gallery-image-5848739667255.jpg', 11),
(77, 3, 'gallery-image-584873967db44.jpg', 12),
(78, 3, 'gallery-image-58487396921aa.jpg', 13),
(79, 3, 'gallery-image-58487396aa7b3.jpg', 14),
(80, 3, 'gallery-image-58487396b71a8.jpg', 15),
(81, 3, 'gallery-image-58487396caf6d.jpg', 16),
(82, 3, 'gallery-image-58487396dde4f.jpg', 17),
(83, 3, 'gallery-image-58487396f1798.jpg', 18),
(84, 3, 'gallery-image-5848739711bec.jpg', 19),
(85, 3, 'gallery-image-5848739728825.jpg', 20),
(86, 3, 'gallery-image-584873bd9a46c.jpg', 21),
(87, 3, 'gallery-image-584873bdb86bc.jpg', 22),
(88, 3, 'gallery-image-584873bdce174.jpg', 23),
(89, 3, 'gallery-image-584873bde3722.jpg', 24),
(90, 3, 'gallery-image-584873be0cb95.jpg', 25),
(91, 3, 'gallery-image-584873be2971a.jpg', 26),
(92, 3, 'gallery-image-584873be448fa.jpg', 27),
(93, 3, 'gallery-image-584873be58c65.jpg', 28),
(94, 3, 'gallery-image-584873be77027.jpg', 29),
(95, 3, 'gallery-image-584873be92953.jpg', 30),
(96, 3, 'gallery-image-584873e506f89.jpg', 31),
(97, 3, 'gallery-image-584873e5216cb.jpg', 32),
(98, 3, 'gallery-image-584873e53575a.jpg', 33),
(99, 3, 'gallery-image-584873e54bede.jpg', 34),
(100, 3, 'gallery-image-584873e563774.jpg', 35),
(101, 3, 'gallery-image-584873e57f061.jpg', 36),
(102, 3, 'gallery-image-584873e598aa3.jpg', 37),
(103, 3, 'gallery-image-584873e5ad244.jpg', 38),
(104, 3, 'gallery-image-584873e5c1fe5.jpg', 39),
(105, 3, 'gallery-image-584873e5d6068.jpg', 40),
(106, 3, 'gallery-image-58487439eeedc.jpg', 41),
(107, 3, 'gallery-image-5848743a15bff.jpg', 42),
(108, 3, 'gallery-image-5848743a2a6b5.jpg', 43),
(109, 3, 'gallery-image-5848743a3e87c.jpg', 44),
(110, 3, 'gallery-image-5848743a58247.jpg', 45),
(111, 3, 'gallery-image-5848743a6e835.jpg', 46),
(112, 3, 'gallery-image-5848743a81fab.jpg', 47),
(113, 3, 'gallery-image-5848743a96fe4.jpg', 48),
(114, 3, 'gallery-image-5848743aabad8.jpg', 49),
(115, 3, 'gallery-image-5848743ac1d00.jpg', 50),
(116, 3, 'gallery-image-5848743ada4ea.jpg', 51),
(117, 3, 'gallery-image-5848743aee27a.jpg', 52),
(118, 3, 'gallery-image-5848743b0f205.jpg', 53),
(119, 3, 'gallery-image-5848743b2371f.jpg', 54),
(120, 3, 'gallery-image-5848743b3bbfe.jpg', 55),
(121, 3, 'gallery-image-5848743b52a4c.jpg', 56),
(122, 3, 'gallery-image-5848743b63cf6.jpg', 57),
(123, 3, 'gallery-image-5848743b7ed6b.jpg', 58),
(124, 3, 'gallery-image-5848743b9bbb7.jpg', 59),
(125, 3, 'gallery-image-5848743bb7425.jpg', 60),
(126, 3, 'gallery-image-58487469bb40e.jpg', 61),
(127, 3, 'gallery-image-58487469d58d6.jpg', 62),
(128, 3, 'gallery-image-58487469f1f52.jpg', 63),
(129, 3, 'gallery-image-5848746a12ee8.jpg', 64),
(130, 3, 'gallery-image-5848746a25a25.jpg', 65),
(131, 3, 'gallery-image-5848746a3cf83.jpg', 66),
(132, 3, 'gallery-image-5848746a5169d.jpg', 67),
(133, 3, 'gallery-image-5848746a69c12.jpg', 68),
(134, 3, 'gallery-image-5848746a86547.jpg', 69),
(135, 3, 'gallery-image-5848746aa2f20.jpg', 70),
(136, 3, 'gallery-image-5848746ac32d8.jpg', 71),
(137, 3, 'gallery-image-5848746adfb9d.jpg', 72),
(138, 3, 'gallery-image-5848746b08ee2.jpg', 73),
(139, 3, 'gallery-image-5848746b2432b.jpg', 74),
(140, 3, 'gallery-image-5848746b4159c.jpg', 75),
(141, 3, 'gallery-image-5848748d603ad.jpg', 76),
(142, 3, 'gallery-image-5848748d7c265.jpg', 77),
(143, 3, 'gallery-image-5848748d8ee76.jpg', 78),
(144, 3, 'gallery-image-5848748da5d3b.jpg', 79),
(145, 3, 'gallery-image-5848748dbb870.jpg', 80),
(146, 3, 'gallery-image-5848748dcff3f.jpg', 81),
(147, 3, 'gallery-image-5848748decd70.jpg', 82),
(148, 3, 'gallery-image-5848748e0d7d4.jpg', 83),
(149, 3, 'gallery-image-5848748e26998.jpg', 84),
(150, 3, 'gallery-image-5848748e3f068.jpg', 85),
(151, 3, 'gallery-image-5848748e5338a.jpg', 86),
(152, 3, 'gallery-image-5848748e66b4b.jpg', 87),
(153, 3, 'gallery-image-5848748e7c28b.jpg', 88),
(154, 3, 'gallery-image-5848748e8fa0b.jpg', 89),
(155, 3, 'gallery-image-5848748ea529e.jpg', 90),
(156, 4, 'gallery-image-586a8c2c18def.png', 1),
(157, 4, 'gallery-image-586a8c2c53eef.png', 2),
(158, 4, 'gallery-image-586a8c2c6d963.png', 3),
(159, 2, 'gallery-image-5873a6368f42c.jpg', 1),
(160, 2, 'gallery-image-5873a636c5c28.jpg', 2),
(161, 2, 'gallery-image-5873a636db15d.jpg', 3),
(162, 2, 'gallery-image-5873a636f173f.jpg', 4),
(163, 2, 'gallery-image-5873a6370f85d.jpg', 6),
(164, 2, 'gallery-image-5873a63721a0c.jpg', 5),
(165, 2, 'gallery-image-5873a6372b8c9.jpg', 7),
(166, 2, 'gallery-image-5873a6373811d.jpg', 8),
(168, 5, 'gallery-image-5873bbdb77cc9.jpg', 1),
(169, 5, 'gallery-image-5873bbdb94669.jpg', 2),
(170, 5, 'gallery-image-5873bbdba6788.jpg', 3);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `menu_item`
--

CREATE TABLE `menu_item` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `route_parameters` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `onclick` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `blank` tinyint(1) DEFAULT NULL,
  `arrangement` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `menu_item`
--

INSERT INTO `menu_item` (`id`, `parent_id`, `location`, `type`, `title`, `route`, `route_parameters`, `url`, `onclick`, `blank`, `arrangement`) VALUES
(1, NULL, 'menu_top', 'url', 'O mnie', NULL, NULL, '#about-me', NULL, 0, 1),
(2, NULL, 'menu_top', 'url', 'Gabinet', NULL, NULL, '#cabinet', NULL, 0, 2),
(3, NULL, 'menu_top', 'url', 'Oferta', NULL, NULL, '#offer', NULL, 0, 3),
(4, NULL, 'menu_top', 'url', 'Aktualności', NULL, NULL, '#news', NULL, 0, 4),
(5, NULL, 'menu_top', 'url', 'Kontakt', NULL, NULL, '#map-section', NULL, 0, 5);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `show_main` tinyint(1) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `old_slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_short_generate` tinyint(1) DEFAULT NULL,
  `content_short` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_generate` tinyint(1) DEFAULT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `published_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `news`
--

INSERT INTO `news` (`id`, `gallery_id`, `show_main`, `title`, `slug`, `old_slug`, `content_short_generate`, `content_short`, `content`, `photo`, `seo_generate`, `seo_title`, `seo_keywords`, `seo_description`, `created_at`, `updated_at`, `published_at`) VALUES
(1, 4, NULL, 'Density Platelet Gel', 'density-platelet-gel', 'density-platelet-gel', 0, 'Density Platelet Gel to najnowszej generacji zestawy do przygotowywania osocza bogatopłytkowego.', '<p>\r\n	<img alt="" src="/upload/pliki/images/platelet%20gel/opakowanie.png" style="width: 496px; height: 438px;" /></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="color:#484848;"><strong>Density Platelet Gel </strong>to najnowszej generacji zestawy do przygotowywania osocza bogatopłytkowego, zaawansowane technologicznie, dzięki czemu charakteryzują się wysoką jakością i wydajnością, a także bezpieczeństwem zastosowania.</span></p>\r\n<p>\r\n	<span style="color:#484848;">Density Platelet Gel spełniają najwyższe standardy bezpieczeństwa związane z ich użytkowaniem, ochroną zdrowia oraz środowiska.</span></p>\r\n<p>\r\n	<span style="color:#484848;">Posiadają oznakowanie CE (93/42/EEC zaktualizowane dyrektywą 2007/47/CEE), a także liczne certyfikaty ISO.</span></p>\r\n<p>\r\n	<span style="color:#484848;">Zabiegi osoczem bogatopłytkowym stosuje się w celu naturalnego odmłodzenia i regeneracji sk&oacute;ry. Osocze bogatopłytkowe zawiera liczne czynniki wzrostu, kt&oacute;re stymulują powstawanie nowego kolagenu i naczyń krwionośnych, a także przyspieszają powstawanie nowych kom&oacute;rek nask&oacute;rka.</span></p>\r\n<p>\r\n	<span style="color:#484848;">W efekcie sk&oacute;ra się regeneruje, następuje poprawa jej elastyczności, gęstości i napięcia, natomiast fałdy i zmarszczki ulegają wygładzeniu. Sk&oacute;ra staje się lepiej ukrwiona i nawilżona, wyraźnie młodsza, a twarz nabiera blasku.</span></p>\r\n<p>\r\n	<span style="color:#484848;">W celu otrzymania osocza bogatopłytkowego konieczne jest pobranie od pacjenta pewnej objętości krwi pełnej. Ilość pobranej krwi zależy od objętości osocza bogatopłytkowego jaką zamierza się uzyskać. Najczęściej potrzebne jest od 9 do 18 ml krwi w celu otrzymania ok. 5 ml osocza bogatopłytkowego.</span></p>\r\n<p>\r\n	<span style="color:#484848;">Krew zostaje pobrana pr&oacute;żniowo do fiolki z antykoagulantem i żelem separującym. Wirujemy ją 5 minut w 4000 RPM w wir&oacute;wce np. MPW 56 ze stałym rotorem. Płytki krwi zostają oddzielone od pozostałych składnik&oacute;w, a następnie zagęszczone i zawieszone w osoczu krwi.</span></p>', 'news-image-5834a80570a3c.png', 0, 'News1', 'News1, Lorem, ipsum, dolor, amet, consectetur, adipisicing, elit, eiusmod, tempor', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.', '2016-10-24 14:55:50', '2017-01-09 17:40:03', '2016-10-24 14:55:00'),
(2, 5, NULL, 'Wypełniacze Princess', 'wypelniacze-princess', 'wypelniacze-princess', 0, 'Stosujemy najwyższej jakości wypełniacze Princess ®', '<p>\r\n	<img alt="" src="/upload/pliki/images/princess/FB_IMG_1476359421076.jpg" style="width: 525px; height: 413px;" /></p>\r\n<p>\r\n	<span style="color:#484848;"><strong>&bdquo;Bezpieczeństwo przede wszystkim&rdquo;</strong> to nasze motto. Składniki gamy Princess &reg; gwarantują stuprocentowe bezpieczeństwo: nasze produkty mają najniższą zawartość czynnik&oacute;w wiążących krzyżowo wśr&oacute;d produkt&oacute;w dostępnych obecnie na rynku, dzięki zastosowaniu unikalnej metody oczyszczania usuwającej resztki czynnika sieciującego BDDE poniżej poziomu wykrywalności (&amp;lt; 1 ppm). Dzięki podw&oacute;jnej sterylizacji wszystkich produkt&oacute;w iwykorzystaniu oczyszczonego kwasu hialuronowego do syntezy S.M.A.R.T. możemy być pewni, że nasze produkty są całkowicie bezpieczne. Gama składa się z 3 produkt&oacute;w:</span></p>\r\n<ul>\r\n	<li>\r\n		<span style="color:#484848;"><strong>Princess &reg; Filler </strong>&ndash; wskazany jest do iniekcji do sk&oacute;ry właściwej i tkanki podsk&oacute;rnej w celu korekty umiarkowanych i znacznych zmarszczek, fałd na twarzy, wypełniania ust oraz blizn.</span></li>\r\n	<li>\r\n		<span style="color:#484848;"><strong>Princess &reg; Volume</strong> &ndash; zawiera mocno wiązany krzyżowo żel z kwasu hialuronowego. Idealny do wypełniania głębszych zmarszczek i przywracania objętości sk&oacute;ry.</span></li>\r\n	<li>\r\n		<span style="color:#484848;"><strong>Princess &reg; Rich</strong> &ndash; nieusieciowany kwas hialuronowy z gliceryną do mezoterapii. Produkty dają naturalne i długotrwałe efekty od 6 do 12 miesięcy.</span></li>\r\n</ul>', 'news-image-582caa12c10e4.jpeg', 0, 'News2', 'News2, Lorem, ipsum, dolor, amet, consectetur, adipisicing, elit', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.', '2016-10-24 14:56:14', '2017-01-09 17:36:02', '2016-10-23 14:55:00'),
(3, NULL, NULL, 'Nowatorski system liftingu bezoperacyjnego Jeisys ULTRAcel.', 'nowatorski-system-liftingu-bezoperacyjnego-jeisys-ultracel', 'nowatorski-system-liftingu-bezoperacyjnego-jeisys-ultracel', 0, 'Najbardziej zaawansowana technologia nie-chirurgicznego liftingu ciała.', '<p>\r\n	Nowatorski system liftingu bezoperacyjnego<strong> Jeisys ULTRAcel.</strong></p>\r\n<p>\r\n	Najbardziej zaawansowana technologia nie-chirurgicznego liftingu ciała.</p>\r\n<p>\r\n	Najwyższa efektywność poprzez połączenie trzech czynnik&oacute;w aktywnych - technologii FRM, FUS oraz GFR.</p>\r\n<p>\r\n	<strong>FUS - Skoncentrowane ultradźwięki</strong></p>\r\n<p style="text-align: center;">\r\n	<strong><img alt="FUS końcówka" src="/upload/pliki/images/ultracel/fus_koncowki.png" style="width: 269px; height: 149px;" /></strong></p>\r\n<p>\r\n	ULTRAcel z częstotliwością 7MHz dostarcza HIFU do warstwy tłuszczu i warstwy SMAS przez dwa rodzaje końc&oacute;wek: 3.0mm and 4.5mm.</p>\r\n<p>\r\n	<img alt="" src="/upload/pliki/images/ultracel/fus_skin.png" style="width: 525px; height: 317px;" /></p>\r\n<p>\r\n	<strong>FRM - Frakcyjny mikro-igłowy system RF</strong></p>\r\n<p>\r\n	<strong><img alt="" src="/upload/pliki/images/ultracel/ULTRAcel%20ESLT%20%20%20Medical.png" style="width: 525px; height: 232px;" /></strong></p>\r\n<p>\r\n	Unikalna technika leczenia sk&oacute;ry bez niszczenia nask&oacute;rka dzięki szybkiej penetracji specjalnie zaprojektowanymi izolowanymi mikro-igłami.</p>\r\n<p>\r\n	<strong>GFR - System RF do zabieg&oacute;w frakcyjnych</strong></p>\r\n<p style="text-align: center;">\r\n	<strong><img alt="" src="/upload/pliki/images/ultracel/gfr.png" style="width: 404px; height: 115px;" /></strong></p>\r\n<p>\r\n	Energia RF dostarczona jako patern frakcyjny powoduje wytworzenie w sk&oacute;rze siatki mikrouszkodzeń termicznych oraz r&oacute;wnomierne podgrzanie wł&oacute;kien kolagenowych, w celu ich remodelingu.</p>\r\n<p>\r\n	<strong>GFR - końc&oacute;wka zabiegowa</strong></p>\r\n<p style="text-align: center;">\r\n	<strong><img alt="" src="/upload/pliki/images/ultracel/gfr_koncowki.png" style="width: 256px; height: 173px;" /></strong></p>\r\n<p>\r\n	GFRTM umożliwia leczenie zar&oacute;wno płytkich i małych jak i głębokich i dużych obszar&oacute;w zabiegowych. Każda końc&oacute;wka GFRTM jest sterylizowana. W zestawie GFRTM jest karta chipowa oraz końc&oacute;wka.</p>\r\n<p style="text-align: center;">\r\n	<img alt="" src="/upload/pliki/images/ultracel/gfr_skin.png" style="width: 300px; height: 134px;" /></p>\r\n<p>\r\n	System GFRTM zawiera generator o częstotliwości 6MHz kt&oacute;ry dostarcza prąd RF przez specjalne końc&oacute;wki zabiegowe. Końc&oacute;wka GFR działa na określony obszar nask&oacute;rka. Frakcyjny system RF dostarcza energię o fakturze siatkowej do nask&oacute;rka oraz tworzy jednorodną strefę termiczną<br />\r\n	w g&oacute;rnej części sk&oacute;ry właściwej.</p>', 'news-image-5837ffc5823cf.jpeg', 0, 'News3', 'News3, Lorem, ipsum, dolor, amet, consectetur, adipisicing, elit, eiusmod, tempor', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.', '2016-10-24 14:56:45', '2017-01-09 17:17:59', '2016-10-22 14:56:00');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `page`
--

CREATE TABLE `page` (
  `id` int(11) NOT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `old_slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_short_generate` tinyint(1) DEFAULT NULL,
  `content_short` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `seo_generate` tinyint(1) DEFAULT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `page`
--

INSERT INTO `page` (`id`, `gallery_id`, `title`, `slug`, `old_slug`, `content_short_generate`, `content_short`, `content`, `seo_generate`, `seo_title`, `seo_keywords`, `seo_description`, `created_at`, `updated_at`, `photo`) VALUES
(1, 3, 'O mnie', 'o-mnie', 'o-mnie', 0, NULL, '<p>\r\n	Studia medyczne ukończyłam na Wydziale Lekarskim Akademii Medycznej w Krakowie w 1992 roku. Następnie rozpoczęłam specjalizację na Oddziale Dermatologii Szpitala Wojew&oacute;dzkiego Nr 1 w Rzeszowie. W 1996 uzyskałam pierwszy stopień specjalizacji, a w 2000 zdałam z wyr&oacute;żnieniem drugi stopień specjalizacji z zakresu dermatologii i wenerologii.</p>\r\n<p>\r\n	Od ukończenia studi&oacute;w praktykuję jako dermatolog, pracując z dorosłymi i dziećmi. Od blisko 20 lat zajmuję się r&oacute;wnież medycyną estetyczną.</p>\r\n<p>\r\n	Liczne kursy , szkolenia&nbsp; oraz uczestnictwo w konferencjach krajowych i zagranicznych umozliwiają mi stałe podnoszenie swoich kwalifikacji i dostęp do bieżącej wiedzy oraz wprowadzanie nowych technologii i produkt&oacute;w.</p>\r\n<p>\r\n	Jestem członkiem <strong>Polskiego Towarzystwa Dermatologicznego</strong> oraz <strong>Stowarzyszenia Dermatolog&oacute;w Estetycznych.</strong></p>', 0, 'O mnie', 'mnie, perspiciatis, unde, omnis, iste, natus, error, voluptatem, accusantium, doloremque', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore...', '2016-10-24 13:36:24', '2016-12-07 21:49:12', NULL),
(2, 2, 'Gabinet', 'gabinet', 'gabinet', 0, NULL, '<p>\r\n	<span style="color:#484848;"><font face="Open Sans, sans-serif"><span style="background-color: rgb(255, 255, 255);">Nowoczesny i świetnie wyposażony gabinet dermatologii i medycyny estetycznej zapewnia gwarancję najwyższego komfortu.</span></font></span></p>', 0, 'Gabinet', 'Gabinet, perspiciatis, unde, omnis, iste, natus, error, voluptatem, accusantium, doloremque', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore...', '2016-10-24 13:36:59', '2016-11-28 21:24:33', NULL),
(3, NULL, 'Oferta', 'oferta', 'oferta', 0, NULL, '<p>\r\n	<span style="color:#484848;"><font face="Open Sans, sans-serif"><span style="background-color: rgb(255, 255, 255);">Zapraszam serdecznie do zapoznania się z ofertą świadczonych przeze mnie usług. W przypadku jakichkolwiek pytań zachęcam do kontaktu.</span></font></span></p>', 0, 'Oferta', 'Oferta, perspiciatis, unde, omnis, iste, natus, error, voluptatem, accusantium, doloremque', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore...', '2016-10-24 13:37:21', '2016-11-19 10:34:15', NULL),
(4, NULL, 'Medycyna estetyczna', 'medycyna-estetyczna', 'medycyna-estetyczna', 0, NULL, '<p>\r\n	<span style="font-size:16px;"><span style="color:#484848;"><strong>Medycyna </strong>estetyczna stoi na pograniczu kosmetyki i chirurgii plastycznej. Pomaga utrzymać naszą &nbsp;sk&oacute;rę w dobrej kondycji przez wiele lat. Proponuje nam szereg zabieg&oacute;w powstrzymujących starzenie sk&oacute;ry. Jest dziedziną bardzo prężnie rozwijającą się, kt&oacute;rej rzesza odbiorc&oacute;w wciąż rośnie. Korzystając z zabieg&oacute;w dobranych przez specjalistę możemy uzyskać odmłodzenie sk&oacute;ry, poprawę rys&oacute;w i owalu twarzy, poprawę sylwetki oraz usunąć niedoskonałości. Zabiegi wykonywane są w komfortowych warunkach, szybko i w większości przypadk&oacute;w nie wyłączają nas z życia codziennego.</span></span></p>\r\n<p class="emphasize">\r\n	<span style="color:#484848;">Oferujemy szereg zabieg&oacute;w z dziedziny medycyny estetycznej:</span></p>\r\n<ul>\r\n	<li>\r\n		<span style="color:#484848;">Wypełnianie zmarszczek kwasem hialuronowym</span></li>\r\n	<li>\r\n		<span style="color:#484848;">Powiększanie i modelowanie ust kwasem hialuronowym</span></li>\r\n	<li>\r\n		<span style="color:#484848;">Modelowanie policzk&oacute;w, brody, poprawa konturu&nbsp;<span style="color:#484848;">twarzy</span></span></li>\r\n	<li>\r\n		<span style="color:#484848;"><span style="color:#484848;">Lifting wolumetryczny za pomocą stabilizowanego kwasu&nbsp;<span style="color:#484848;">hialuronowego</span></span></span></li>\r\n	<li>\r\n		<span style="color:#484848;">Usuwanie zmarszczek mimicznych toksyną botulinową (Botox, Azzalurae)</span></li>\r\n	<li>\r\n		<span style="color:#484848;">Mezoterapia - rewitalizacja sk&oacute;ry twarzy, szyi, dekoltu; cellulit, rozstępy, łysienie, przebarwienia i inne</span></li>\r\n	<li>\r\n		<span style="color:#484848;">Lipoliza iniekcyjna (modelowanie sylwetki, redukcja tkanki tłuszczowej oraz cellulitu)</span></li>\r\n	<li>\r\n		<span style="color:#484848;">Nici liftingujace PDO</span></li>\r\n	<li>\r\n		<span style="color:#484848;">Osocze bogatopłytkowe</span></li>\r\n	<li>\r\n		<span style="color:#484848;">Peelingi medyczne (rewitalizacja, leczenie trądziku, przebarwień, inne)</span></li>\r\n	<li>\r\n		<span style="color:#484848;">Leczenie przebarwień i melasmy (Dermamelan, Cosmelan, Aquashine)</span></li>\r\n	<li>\r\n		<span style="color:#484848;">Leczenie nadpotliwośći</span></li>\r\n	<li>\r\n		<span style="color:#484848;">Laserowe usuwanie naczynek</span></li>\r\n	<li>\r\n		<span style="color:#484848;">Laserowe usuwanie blizn</span></li>\r\n	<li>\r\n		<span style="color:#484848;">Laser frakcyjny Co2 - odmładzanie laserowe, leczenie zmarszczek, blizn</span></li>\r\n	<li>\r\n		<span style="color:#484848;">Zabiegi falami radiowymi RF</span></li>\r\n	<li>\r\n		<span style="color:#484848;">Thermage</span></li>\r\n	<li>\r\n		<span style="color:#484848;">Radiofrekwencja mikroigłowa</span></li>\r\n	<li>\r\n		<span style="color:#484848;">Ultradźwięki - bezinwazyjny lifting i ujędrnianie sk&oacute;ry twarzy, szyi, dekoltu.</span></li>\r\n	<li>\r\n		<span style="color:#484848;">Ultracel&trade; - nowatorski &nbsp;multisystem do liftingu bezoperacyjnego (napinanie i &nbsp;pojędrnianie sk&oacute;ry, regeneracja, modelowanie, leczenie zmarszczek, utraty napięcie, rozstęp&oacute;w, blizn, nadpotliwości)</span></li>\r\n</ul>\r\n<div>\r\n	<span style="color:#484848;">*frakcyjny mikroigłowy system RF (FRM TM)</span></div>\r\n<div>\r\n	<span style="color:#484848;">*skoncentrowane ultradźwięki (FUS TM)</span></div>\r\n<div>\r\n	<span style="color:#484848;">*frakcyjny system RF (GFR TM)xt 2</span></div>', 0, 'Medycyna estetyczna', 'Medycyna, estetyczna, stoi, pograniczu, kosmetyki, chirurgii, plastycznej, Pomaga, utrzymać, naszą', 'Medycyna estetyczna stoi na pograniczu kosmetyki i chirurgii plastycznej. Pomaga utrzymać naszą skórę w dobrej kondycji przez wiele lat. Proponuje nam...', '2016-10-25 12:21:59', '2017-01-09 15:09:02', 'page-image-5836fb8327671.jpeg'),
(5, NULL, 'Dermatologia i dermatochirurgia', 'dermatologia-i-dermatochirurgia', 'dermatologia-i-dermatochirurgia', 0, NULL, '<p>\r\n	<span style="color:#484848;">Dermatologia i dermatochirurgia to m.in.:</span></p>\r\n<ul>\r\n	<li>\r\n		<span style="color:#484848;">Diagnostyka i leczenie chor&oacute;b sk&oacute;ry dorosłych i dzieci:</span>\r\n		<ul>\r\n			<li>\r\n				<span style="color:#484848;">choroby alergiczne - testy punktowe pokarmowe oraz wziewne,</span></li>\r\n			<li>\r\n				<span style="color:#484848;">trądzik, trądzik r&oacute;żowaty,</span></li>\r\n			<li>\r\n				<span style="color:#484848;">przewlekłe choroby sk&oacute;ry,</span></li>\r\n			<li>\r\n				<span style="color:#484848;">choroby infekcyjne - grzybice, choroby bakteryjne i wirusowe,</span></li>\r\n			<li>\r\n				<span style="color:#484848;">łuszczyca,</span></li>\r\n			<li>\r\n				<span style="color:#484848;">inne.</span></li>\r\n		</ul>\r\n	</li>\r\n	<li>\r\n		<span style="color:#484848;">Dermatoskopia zmian barwnikowych.</span></li>\r\n	<li>\r\n		<span style="color:#484848;">Usuwanie brodawek wirusowych, wł&oacute;kniak&oacute;w , znamion kom&oacute;rkowych, innych.</span></li>\r\n	<li>\r\n		<span style="color:#484848;">Kriochirurgia.</span></li>\r\n	<li>\r\n		<span style="color:#484848;">Elektrochirurgia.</span></li>\r\n	<li>\r\n		<span style="color:#484848;">Laser Co2 (usuwanie zmian sk&oacute;rnych, blizn).</span></li>\r\n</ul>', 0, 'Dermatologia i dermatochirurgia', 'Dermatologia, dermatochirurgia, przyugotowaniu', 'W przyugotowaniu...', '2016-10-25 15:34:24', '2016-12-06 23:40:19', 'page-image-5836de084376f.jpeg');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `setting`
--

CREATE TABLE `setting` (
  `id` int(11) NOT NULL,
  `label` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `setting`
--

INSERT INTO `setting` (`id`, `label`, `description`, `value`) VALUES
(1, 'seo_description', 'Domyślna wartość meta tagu "description"', 'Nowoczesny i świetnie wyposażony gabinet dermatologii i medycyny estetycznej zapewnia Państwu gwarancję najwyższego komfortu.'),
(2, 'seo_keywords', 'Domyślna wartość meta tagu "keywords"', NULL),
(3, 'seo_title', 'Domyślna wartość meta tagu "title"', 'Anita Kazieńko-Jóźwik / dermatologia i medycyna estetyczna Rzeszów'),
(8, 'link_referencje', 'Adres do odnośnika do referencji', 'https://www.znanylekarz.pl/anita-kazienko-jozwik/dermatolog-lekarz-medycyny-estetycznej/rzeszow'),
(9, 'telefon', 'Telefon', '603 126 019'),
(10, 'adres', 'Adres', 'ul. Zenitowa 15, 35-301 Rzeszów'),
(11, 'marker', 'Współrzędne', '50.0373114,22.0359413');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `label` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo_width` int(11) NOT NULL,
  `photo_height` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `slider_photo`
--

CREATE TABLE `slider_photo` (
  `id` int(11) NOT NULL,
  `slider_id` int(11) DEFAULT NULL,
  `filename` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `arrangement` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `salt` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:json_array)',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `salt`, `password`, `active`, `last_login`, `roles`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'biuro@lemonadestudio.pl', 'e40e0bc6f3ebacbed6e0e19b772845a8', 'Rd/3Txoa82/D9hi5AUbzSaMo0mR7YPs5tVG7TwD12SREfgob9BnzCItP1AhQxf7gi/IHE67DdDYPbMWPZIAROg==', 1, NULL, '["ROLE_ADMIN","ROLE_ALLOWED_TO_SWITCH","ROLE_USER"]', '2015-07-31 13:46:34', NULL),
(2, 'sylwia', 'sylwia13s@gmail.com', 'cab7c00831f2821b6bff0d0b8f97f35b', 'LLTMaZoVhTMV9TgimRDgpiJ/KmANmrPl21goPwbrnvwp9p1yWH8AVg/qmha9Ca0SxWk2S9Ajd0d/cxrEtjLsDw==', 1, NULL, '["ROLE_ADMIN"]', '2016-12-07 20:48:41', NULL);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery_photo`
--
ALTER TABLE `gallery_photo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_F02A543B4E7AF8F` (`gallery_id`);

--
-- Indexes for table `menu_item`
--
ALTER TABLE `menu_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_D754D550727ACA70` (`parent_id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_1DD399504E7AF8F` (`gallery_id`);

--
-- Indexes for table `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_140AB6204E7AF8F` (`gallery_id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_9F74B898EA750E8` (`label`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_CFC71007EA750E8` (`label`);

--
-- Indexes for table `slider_photo`
--
ALTER TABLE `slider_photo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_9203C87C2CCC9638` (`slider_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8D93D649F85E0677` (`username`),
  ADD UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT dla tabeli `gallery_photo`
--
ALTER TABLE `gallery_photo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=171;
--
-- AUTO_INCREMENT dla tabeli `menu_item`
--
ALTER TABLE `menu_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT dla tabeli `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT dla tabeli `page`
--
ALTER TABLE `page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT dla tabeli `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT dla tabeli `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `slider_photo`
--
ALTER TABLE `slider_photo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `gallery_photo`
--
ALTER TABLE `gallery_photo`
  ADD CONSTRAINT `FK_F02A543B4E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `gallery` (`id`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `menu_item`
--
ALTER TABLE `menu_item`
  ADD CONSTRAINT `FK_D754D550727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `menu_item` (`id`);

--
-- Ograniczenia dla tabeli `news`
--
ALTER TABLE `news`
  ADD CONSTRAINT `FK_1DD399504E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `gallery` (`id`);

--
-- Ograniczenia dla tabeli `page`
--
ALTER TABLE `page`
  ADD CONSTRAINT `FK_140AB6204E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `gallery` (`id`);

--
-- Ograniczenia dla tabeli `slider_photo`
--
ALTER TABLE `slider_photo`
  ADD CONSTRAINT `FK_9203C87C2CCC9638` FOREIGN KEY (`slider_id`) REFERENCES `slider` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
