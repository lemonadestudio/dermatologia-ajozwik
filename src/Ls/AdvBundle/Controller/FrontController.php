<?php

namespace Ls\AdvBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
/**
 * Adv controller.
 *
 */
class FrontController extends Controller {

    public function indexAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        
        $locale = $request->getLocale();

        $qb = $em->createQueryBuilder();
        $entity = $qb->select('a')
            ->from('LsAdvBundle:Adv', 'a')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();

        $sessionValue = md5($entity->getLink() . $entity->getPhoto());
        
        return $this->render('LsAdvBundle:Front:index.html.twig', array(
            'entity' => $entity,
            'session_value' => $sessionValue
        ));
    }

    public function closeAction(Request $request, $value) {

    	if($request->isXmlHttpRequest()) {
    		$session = new Session();
           	$session->set('popup',  $value);
        }
        else {
        	throw $this->createNotFoundException('Ajax request only');
        }

        return new Response('OK');
    }
}
