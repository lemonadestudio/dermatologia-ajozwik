<?php

namespace Ls\AdvBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Ls\CoreBundle\Utils\Tools;
use Ls\AdvBundle\Entity\Adv;

class AdvUpdater implements EventSubscriber {

    public function getSubscribedEvents() {
        return array(
            'postRemove',
        );
    }


    public function postRemove(LifecycleEventArgs $args) {
        $entity = $args->getEntity();

        if ($entity instanceof Adv) {
            $entity->deletePhoto();
        }
    }

}