<?php

namespace Ls\PageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Page controller.
 *
 */
class FrontController extends Controller {

    /**
     * Finds and displays a Page entity.
     *
     */
    public function showAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
		$slug = $request->get('slug');

        $entity = $em->createQueryBuilder()
            ->select('p', 'g')
            ->from('LsPageBundle:Page', 'p')
            ->where('p.slug = :slug')
            ->leftJoin('p.gallery', 'g')
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getOneOrNullResult();
        
        if (null === $entity) {
            throw $this->createNotFoundException('Unable to find Page entity.');
        }

		$response = array(
            'html' => iconv("UTF-8", "UTF-8//IGNORE", $this->render('LsPageBundle:Front:show.html.twig', array(
                'entity' => $entity,
            ))->getContent())
        );
		
		return new JsonResponse($response);
    }

}
