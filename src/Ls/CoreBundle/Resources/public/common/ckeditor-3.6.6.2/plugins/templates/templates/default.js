﻿/*
Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.addTemplates('default',{imagesPath:CKEDITOR.getUrl(CKEDITOR.plugins.getPath('templates')+'templates/images/'),templates:[
        {title:'Jedna kolumna',
            image:'',
            description:'Zawartość w jednej kolumnie.',
            html:'<table class="news-inside-table" cellspacing="0" cellpadding="0" style="width:100%" border="0"><tr style="vertical-align: top;"><td>Tekst</td></tr></table>'},
        {title:'Dwie kolumny',image:'',
            description:'Układ dwukolumnowy',
            html:'<table class="news-inside-table" cellspacing="0" cellpadding="0" style="width:100%" border="0"><tr style="vertical-align: top;"><td style="width: 50%; padding-right: 60px;">Tekst</td><td style="width: 50%;">Tekst</td></tr></table>'},
]});
