$(document).ready(function () {
	// scrollowanie
	$("a.link, #upper-nav ul li a, .navbar ul li a, .sticky-nav ul li a, .navbar-sticky ul li a").click(function () {
		var elementClick = $(this).attr("href");
		var body = $('body, html');
		var destination = $(elementClick);
		var distance = $(destination).offset().top;
		if (elementClick == "#map-section") {
			$(body).animate({scrollTop: distance - 55}, 1100);
			$('.navbar-sticky .rwd-nav').fadeOut();
            $.stellar({
                horizontalScrolling: false,
                verticalScrolling: true,
                hideDistantElements: true,
            });
		} else if (elementClick == "#offer") {
			$(body).animate({scrollTop: distance - 55}, 1100);
			$('.navbar-sticky .rwd-nav').fadeOut();
            $.stellar({
                horizontalScrolling: false,
                verticalScrolling: true,
                hideDistantElements: true,
            });
		} else if (elementClick == "#about-me") {
			$(body).animate({scrollTop: distance - 55}, 1100);
			$('.navbar-sticky .rwd-nav').fadeOut();
            $.stellar({
                horizontalScrolling: false,
                verticalScrolling: true,
                hideDistantElements: true,
            });
		} else {
			$(body).animate({scrollTop: distance}, 1100);
			$('.navbar-sticky .rwd-nav').fadeOut();
            $.stellar({
                horizontalScrolling: false,
                verticalScrolling: true,
                hideDistantElements: true,
            });
		}
		return false;
	});
	
	//Wywołania
    $(document).scroll(function() {
        var distance = $('#offer').offset().top;
        var diff = Math.abs($(window).scrollTop() - distance);
          if(diff < 56){
            $.stellar({
                horizontalScrolling: false,
                verticalScrolling: true,
                hideDistantElements: true,
            });
          }
    });
    $.stellar({
        horizontalScrolling: false,
        verticalScrolling: true,
        hideDistantElements: true,
    });
	$('#content #offer #offer-loader, #content #news #news-loader').hide();
    thumbPager();
    
    var fancyGallery = $(".certificates-container ul").find("a");
    fancyGallery.attr("rel","gallery").fancybox({
        type: "image"
    });
    $('#certificate-trigger').on('click', function() {
        fancyGallery.eq(0).click(); 
    });

});

function thumbPager() {
	var slideshows = $('.gallery-listing .cycle-slideshow').on('cycle-next cycle-prev', function (e, opts) {
		// advance the other slideshow
		slideshows.not(this).cycle('goto', opts.currSlide);
	});

	$('.gallery-listing .slideshow-pager .cycle-slide').click(function () {
		var index = $('.gallery-listing .slideshow-pager').data('cycle.API').getSlideIndex(this);
		slideshows.cycle('goto', index);
	});
}

function thumbPagerOffer() {
	var slideshows = $('.offer-gallery .gallery-offer-listing .cycle-slideshow').on('cycle-next cycle-prev', function (e, opts) {
		// advance the other slideshow
		slideshows.not(this).cycle('goto', opts.currSlide);
	});

	$('.offer-gallery .gallery-offer-listing .slideshow-pager .cycle-slide').click(function () {
		var index = $('.offer-gallery .gallery-offer-listing .slideshow-pager').data('cycle.API').getSlideIndex(this);
		slideshows.cycle('goto', index);
	});
}

function thumbPagerNews() {
	var slideshows = $('.news-gallery .gallery-offer-listing .cycle-slideshow').on('cycle-next cycle-prev', function (e, opts) {
		// advance the other slideshow
		slideshows.not(this).cycle('goto', opts.currSlide);
	});

	$('.news-gallery .gallery-offer-listing .slideshow-pager .cycle-slide').click(function () {
		var index1 = $('.news-gallery .gallery-offer-listing .slideshow-pager').data('cycle.API').getSlideIndex(this);
		slideshows.cycle('goto', index1);
	});
}
// Sprawdza czy element jest widoczny
function isElementInViewport(elem) {
    var $elem = $(elem);

    // Get the scroll position of the page.
    var scrollElem = ((navigator.userAgent.toLowerCase().indexOf('webkit') != -1) ? 'body' : 'html');
    if ($elem == '#circle-about-me') {
        var viewportTop = $(scrollElem).scrollTop() - 360;
    } else {
        var viewportTop = $(scrollElem).scrollTop() - 240;
    }
    var viewportBottom = viewportTop + $(window).height();

    // Get the position of the element on the page.
    var elemTop = Math.round( $elem.offset().top );
    var elemBottom = elemTop + $elem.height();

    return ((elemTop < viewportBottom) && (elemBottom > viewportTop));
}

// Sprawdza, czy pora na uruchomenie animacji
function checkAnimation() {
    var elements = [
        $elem1 = $('#circle-about-me'),
        $elem2 = $('#circle-cabinet'),
        $elem3 = $('#circle-offer')
    ];

    if (isElementInViewport(elements[0])) {
        // Start the animation
        elements[0].addClass('start');
    } else if (isElementInViewport(elements[1])) {
        elements[1].addClass('start');
    } else if (isElementInViewport(elements[2])) {
        elements[2].addClass('start');
    } else {
        return
    }
}

// Capture scroll events
$(window).scroll(function(){
    checkAnimation();
});

//Chowanie i przyklejanie menu na scrollu.
$(window).scroll(function() {
    var scroll = $(window).scrollTop();
	if (window.innerWidth > 1239 ) {
		if (scroll > 10	) {
			$('#header #upper-nav').slideUp();
			$('#header .sticky-nav').slideDown();
		} else {
			$('#header #upper-nav').slideDown();
			$('#header .sticky-nav').slideUp();
	   } 
	} else {
		if (scroll > 10	) {
			$('.navbar').fadeOut();
			$('.navbar-sticky').slideDown();
		} else {
			$('.navbar').fadeIn();
			$('.navbar .rwd-nav').hide();
			$('.navbar-sticky').slideUp();
	   } 
	}
});
	
	//Jednakowa wysokość dla divów.
	if (window.innerWidth > 639) {
		var maxHeight = 0;
		var element = $('#news ul li p.news-excerpt');

		$(element).each(function(){
		   if ($(this).height() > maxHeight) { maxHeight = $(this).height(); }
		});

		$(element).height(maxHeight);
}
	
//Funkcje.	
function changeUri(urlPath){
     $.param.querystring(window.location.href, urlPath);
}
function changeUri(urlPath){
     window.history.back();
}

function toggleNav() {
	$('.rwd-nav').slideDown();
}

function hideNav() {
	$('.rwd-nav').slideUp();
}

function toggleSubpage(slug) {
	var button = $('#'+slug);
	
	var div = $(".subpage-content .container .nano-content");
	var path = $(button).data('path');
	var loading = $('#content #offer #offer-loader');
	var hidden = [
		'.left-side-line',
		'.right-side-line'
	];
	
	var destination = '#offer';
	var distance = $(destination).offset().top;

	$(loading).fadeIn();
	$.ajax({
		type: "GET",
		url: path,
		data: { 
			slug: slug
		},
		dataType: "json", 
		success: function (response) {
			div.html(response.html); 
			$('body, html').animate({scrollTop: distance - 30}, 500);
			$('.subpage-content').show();
            $('#offer .gallery-offer-listing ul').cycle();
			$('#offer .initial-content').hide();
			if (window.innerWidth > 939) {
				$(hidden.join(', ')).fadeOut();
				$("#offer .nano").nanoScroller();
				blockScroll();
                thumbPagerOffer();
			}
		},
		complete: function(){
			$(loading).fadeOut();
		}
	});
}

function closeSubpage() {
	var container = $('.subpage-content');
	var initialContainer = $('#offer .initial-content');
	var hidden = [
		'.left-side-line',
		'.right-side-line'
	];
	
	$(container).hide();
	$(initialContainer).fadeIn();
	$("#offer .nano").nanoScroller({ destroy: true });
	if (window.innerWidth > 940) {
		$(hidden.join(', ')).fadeIn();
	}
	unblockScroll();
}

function blockScroll() {
	$('body').css('overflow-y', 'hidden');
}

function unblockScroll() {
	$('body').css('overflow-y', 'auto');
}

/**
 * Pokazuje galerię 
 */
function showGallery() {
	$('.gallery-listing ul').cycle();
    thumbPager();
	$('.gallery-listing').show();
	$('.cabinet-container').hide();
	$('html, body').animate({
        scrollTop: $(".gallery-listing").offset().top
    }, 0);
    blockScroll();
}

function closeGallery() {
	$('.gallery-listing').hide();
	$('.cabinet-container').show();
	$('.gallery-listing ul').cycle('destroy');
    unblockScroll();
    
}

/**
 * Pokazuje newsy.
 */
function toggleNews() {
	var button = $('a#show-more');
	$("#news .main-listing").slideToggle();
	if (button.text() == 'Pokaż więcej') {
		$(button).text('Pokaż mniej');
	} else {
		$(button).text('Pokaż więcej');
	}
}
	
function goBackNews() {
	var container = $('#one-news-container');
	var initialContainer = $('#news .initial-container');
	var hidden = [
		'.left-side-line',
		'.right-side-line'
	];
	
	$(initialContainer).fadeIn();
	if (window.innerWidth > 940) {
		$(hidden.join(', ')).fadeIn();
	}
	$(container).fadeOut();
    $('ul.cycle-slideshow').cycle('destroy');
	unblockScroll();
}

/**
* Pokazuje jednego news'a
 */
function toggleOneNews(slug) {
	
	var button = $('#'+slug);
	
	var div = $("#one-news-container .container .nano-content");
	var path = $(button).attr('data-path');
	var loading = $('#content #news #news-loader');
	
	var destination = '#news';
	var distance = $(destination).offset().top;
	var hidden = [
		'.left-side-line',
		'.right-side-line'
	];
	

	$(loading).fadeIn();
	$.ajax({
		type: "GET",
		url: path,
		data: { 
			slug: slug
		},
		dataType: "json", 
		success: function (response) {
			div.html(response.html); 
			$('body, html').animate({scrollTop: distance - 50 }, 500);
			$('#one-news-container').fadeIn();
            $('.news-gallery .gallery-offer-listing ul').cycle();
            thumbPagerNews();
			$('#news .initial-container').hide();
			if (window.innerWidth > 939) {
				$("#one-news-container .nano").nanoScroller();
				blockScroll();
				$(hidden.join(', ')).fadeOut();
			} 
		},
		complete: function(){
			$(loading).fadeOut();
		}
	});
		
}
 
function closePopup(path) {
  $.ajax({
      type: "GET",
      url: path,
      dataType: "json", 
      success: function (response) {
      },
      error: function (response) {
      }
  });
}