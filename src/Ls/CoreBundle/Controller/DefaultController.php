<?php

namespace Ls\CoreBundle\Controller;

use Ls\CoreBundle\Helper\AdminBlock;
use Ls\CoreBundle\Helper\AdminDashboard;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller {
    protected $containerBuilder;

    public function indexAction() 
	{
		$mainNews = $this->getNewsRepositoryForHomepage(3);
		$mainNewsForSmallDevice = $this->getNewsRepositoryForHomepage(2);
		$news = $this->getNewsRepository(3);
		$newsForSmallDevice = $this->getNewsRepository(2);
		$Gallery = $this->getGalleryRepository();
        $certificates = $this->getCertificatesRepository();
		
//		Treści na stronie głównej.
		$aboutMe = $this->getPageById(1);
		$cabinet = $this->getPageById(2);
		$offer = $this->getPageById(3);
		
//		Treści podstron oferty
        $medycynaId = 4;
        $dermatologiaId = 5;
        
		$medycyna = $this->getPageById($medycynaId);
		$dermatologia = $this->getPageById($dermatologiaId);
        
        return $this->render('LsCoreBundle:Default:index.html.twig', array(
			'mainNews' => $mainNews,
			'mainNewsForSmallDevice' => $mainNewsForSmallDevice, 
			'news' => $news,
			'newsForSmallDevice' => $newsForSmallDevice,
			'aboutMe' => $aboutMe[0],
			'cabinet' => $cabinet[0],
			'offer' => $offer[0],
			'medycyna' => $medycyna[0],
			'dermatologia' => $dermatologia[0],
			'Gallery' => $Gallery[0],
            'certificates' => $certificates
		));
    }

    public function adminAction() 
	{
        $blocks = $this->container->getParameter('ls_core.admin.dashboard');
        $dashboard = new AdminDashboard();

        foreach($blocks as $block) {
            $parent = new AdminBlock($block['label']);
            $dashboard->addBlock($parent);
            foreach($block['items'] as $item) {
                $service = $this->container->get($item);
                $service->addToDashboard($parent);
            }
        }

        return $this->render('LsCoreBundle:Default:admin.html.twig', array(
            'dashboard' => $dashboard
        ));
    }
	
	/**
	 * Zwraca repozytorium newsów, sortowane po dacie.
	 * @param int $start
	 * @return object
	 */
	public function getNewsRepository($start) 
	{
		$em = $this->getDoctrineManager();
		
		$query = $em->createQuery(
				'SELECT n
				FROM LsNewsBundle:News n
				ORDER BY n.published_at DESC'
			)
			->setFirstResult($start);
		
		$news = $query->getResult();
		
		return $news;
	}
    
    /**
     * Wyciąga galerię certyfikatów.
     * @return object
     */
    public function getCertificatesRepository() 
    {
		$em = $this->getDoctrineManager();
        
        $query = $em->createQuery(
                'SELECT c
                FROM LsGalleryBundle:GalleryPhoto c
                WHERE c.gallery = 3'
                );
        
        $certyficates = $query->getResult();
        
        return $certyficates;
    }
	
	/**
	 * Zwraca repozytorium newsów dla strony głównej, sortowane po dacie i ograniczone do liczby wyników podanej w parametrze.
	 * @param int $id
	 * @return object
	 */
	public function getNewsRepositoryForHomepage($amount) 
	{
		$em = $this->getDoctrineManager();
		
		$query = $em->createQuery(
				'SELECT n
				FROM LsNewsBundle:News n
				ORDER BY n.published_at DESC'
			)
			->setMaxResults($amount);
		
		$mainNews = $query->getResult();
		
		return $mainNews;
	}
	
	/**
	 * Zwraca repozytorium Page
	 * @return object
	 */
	public function getPageRepository() 
	{
		return $this->getDoctrine()
				->getRepository('LsPageBundle:Page');
	}
	
	/**
	 * Zwraca stronę według podanego w parametrze id
	 * @param int $id
	 * @return object
	 */
	public function getPageById($id) 
	{
		$Page = $this->getPageRepository()
			->findBy(array(
				'id' => $id
			));
		
		return $Page;
	}
	
	/**
	 * Zwraca stronę według podanego w parametrze slug
	 * @param string $slug
	 * @return object
	 */
	public function getPageBySlug($slug) 
	{
		$Page = $this->getPageRepository()
			->findBy(array(
				'slug' => $slug
			));
		
		return $Page;
	}
	
	/**	
	 * Zwraca galerię.
	 * @return object
	 */
	public function getGalleryRepository() 
	{
		$em = $this->getDoctrineManager();

        $qb = $em->createQueryBuilder();
        $Gallery = $qb->select('a')
            ->from('LsGalleryBundle:Gallery', 'a')
            ->getQuery()
            ->getResult();
		
		return $Gallery;
	}
    
    public function getDoctrineManager() 
    {
        return $this->getDoctrine()->getManager();
    }
    
    public function KCFinderAction(Request $request)
    {
        $upload_dir = $this->get('kernel')->getRootDir() . '/../web/upload/pliki';

        $_SESSION['KCFINDER']['disabled'] = false;
        $_SESSION['KCFINDER']['uploadDir'] = $upload_dir;

        $getParameters = $request->query->all();

        
        return new RedirectResponse(
            $request->getBasePath() . 
            '/bundles/lscore/common/kcfinder-3.20/browse.php?' . 
            http_build_query($getParameters));
    }
}
